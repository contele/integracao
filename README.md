# Integração de API Contele Gestor de Equipes

## Resumo

A API disponibilizada para integração é desenvolvida baseada no padrão [REST](https://pt.stackoverflow.com/questions/45783/o-que-%C3%A9-rest-e-restful), uma série de endpoints são disponibilizados permitindo controle das entidades fornecidas usando os verbos do padrão. A integração com sistemas internos é de total responsabilidade da empresa contratante e deve ser realizada por desenvolvedores com experiência em APIs REST.

## Primeiros Passos

O primero passo para iniciar a utilização da API do Gestor de Equipes é entrar em contato com o seu consultor contele e confirmar o seu interesse em utulizar a API, após algumas verificações 2 chaves de acesso serão liberadas possibilitando a utilização da API.

## URL Base

É possível interagir com as entidades da aplicação com a URL fornecida:

* [https://integration.contelege.com.br](https://integration.contelege.com.br)

## Documentação

A documentação fornece um mapa completo dos endpoints disponiveis, como utilizá-los, parametros esperados e retorno esperado. Estamos na versão 2 da API, a versão 1 deve ser substituida gradativamente pela 2 que possui mais filtros e mais endpoints.

* [Documentação V2](https://s3-sa-east-1.amazonaws.com/contele-ge-api-doc/v2/index.html)
* [Documentação V1 - DEPRECATED](https://s3-sa-east-1.amazonaws.com/contele-ge-api-doc/v1/index.html)


## Segurança

Para segurança dos dados dos clientes toda requisição deve ser enviada com os cabeçalhos de autenticação que asseguram a identidade e nivel de acesso.

**Cabeçalhos**

- **x-api-key** XXXXXXXXXXXX
- **Authorization** Bearer XXXXXXXXXX


## Exemplos de chamada

### Uso de PostMan

Para realização de chamadas de testes recomendamos a utilização do [PostMan](https://www.getpostman.com/apps), uma ferramenta muito utilizada no mercado para realização de chamadas de teste usando o HTTP.

![exemplo postman](https://bitbucket.org/contele/integracao/raw/9c46190b750fe2e3aae75f18925faaf00bc61fad/docs/postman-get-categorias.png)


### Uso de CURL

Vide a pasta CURL Examples para ver os exemplos disponiveis.

* [Categorias](https://bitbucket.org/contele/integracao/src/master/CURL%20examples/CURL-categorias.md)

# Funcionamento do Swagger Codegen

O Swagger Codegen é uma forma de gerar pontos para a integração de uma maneira automática.

Esta parte da documentação foi inscrita manualmente com o intuíto de explicar a funcionalidade e método de utilização do Swagger Codegen.

A Nossa documentação atual está localizada na api principal com o nome de swagger-api-gateway.yaml

## Swagger

Para visualizar as informações necessárias em cada requisição pode-se utilizar o site:
https://editor.swagger.io/
Ele é um editor que mostra as informações das suas requisições em realtime, assim facilitando o desenvolvimento, além de exportar a documentação do seu projeto de uma maneira prática e rápida.


## Codegen

O Codegen é um script que utiliza a documentação gerada a partir do swagger para escrever automaticamente o código que será utilizado na integração.
Para executar esse script é necessário pelo menos o Java Runtime 8.

## Iniciando no Codegen

### Instalando o Java

É possível instalar o Java Runtime Environment a partir do comando:
`❯ sudo apt install default-jre` 

Após a sua instalação deve-se verificar se o JRE foi instalado corretamente com o comando:
`❯ java -version`

### Obtendo o script para a geração de código

Para gerar o código automaticamente é necessário rodar o script  de geração de código obtido pelo comando:
 
`wget https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/2.2.1/swagger-codegen-cli-2.2.1.jar`

Após isto aparecerá um arquivo em seu repositório. 
Este é o arquivo de configuração e geração.

## Gerando o Código

Para gerar a estrutura de códigos é necessário um script de alguns passos.
1. Inicialmente, por questões de boas práticas, será necessário limpar o repositório em que se encontra o código anteriormente gerado. O motivo dessa alteração é para que caso em sua nova geração haja um arquivo que foi deletado em relação a anterior ele não permaneça na nova versão.
	
	`rm -rf api_contele_ge_v2`
	
2.  Após isso será necessário utilizar o comando de geração levando em consideração alguns parâmetros.
`java -jar swagger-codegen-cli-2.2.1.jar generate \
-i <PATH-FONTE> \
--artifact-version 0.0.1-SNAPSHOT \
-l <LINGUAGEM> \
-o <PATH-FINAL>`
	* PATH-FONTE: É onde esta localizado o arquivo JSON/yml que será utilizado para gerar a estrutura de códigos.
	*  LINGUAGEM: É a forma em que esta estrutura será feita, no caso deste repositório Javascript
	* PATH-FINAL: Seria o local onde a estrutura será gerada, utilizaremos a mesma que removemos no primeiro passo.

* Exemplo: 
`java -jar swagger-codegen-cli-2.2.1.jar generate \
-i codegen-api.json \
--artifact-version 0.0.1-SNAPSHOT \
-l javascript \
-o api_contele_ge_v2`



Fonte: https://github.com/swagger-api/swagger-codegen