# Categorias

## Uso do CURL 

### Parametros requeridos

Verbo:
  
  * `-X GET`


Cabeçalhos:
  
  * `-H "Content-Type: application/json"`
  * `-H "x-api-key: XXXXXXX"`
  * `-H "Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX"`

## Exemplo Final

Execute o comando de exemplo abaixo no terminal para obter a resposta do endpoint.

### GET application/json

    curl \
        -H "Content-Type: application/json" \
        -H "x-api-key: XXXXXXX" \
        -H "Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX" \
        -X GET https://integration.contelege.com.br/v2/categories
    
