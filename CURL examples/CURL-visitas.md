# Visitas

## Uso do CURL 

### Parametros requeridos

Verbo:

  * `-X GET`

Cabeçalhos:

  * `-H "Content-Type: application/json"`
  * `-H "x-api-key: XXXXXXX"`
  * `-H "Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX"`

Parametros QueryString:

  * `perPage - Deve ser usado o valor entre 1 e 100`
  * `timezone - Padrão "America/Sao_Paulo" (Necessário o encoding)`


## Exemplo Final

Execute o comando de exemplo abaixo no terminal para obter a resposta do endpoint.

### GET application/json

    curl \
        -H "Content-Type: application/json" \
        -H "x-api-key: XXXXXXX" \
        -H "Authorization: Bearer XXXXXXXXXXXXXXXXXXXXXX" \
        -X GET https://integration.contelege.com.br/v2/tasks?perPage=50&timezone=%22America/Sao_Paulo%22
    