# ApiConteleGeV2.WebhookApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**webhookSamplePost**](WebhookApi.md#webhookSamplePost) | **POST** /webhook-sample | 


<a name="webhookSamplePost"></a>
# **webhookSamplePost**
> webhookSamplePost(webhookData, authorization)



O weebhook é configurado na página de configurações da sua empresa dentro do Contele Gestor na web. Nele é possivel apontar qual é a url que irá receber as requisições toda vez que alguma ações ocorrer dentro da sua conta. 

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.WebhookApi();

var webhookData = new ApiConteleGeV2.WebhookData(); // WebhookData | Entidade `WebhookData`

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.webhookSamplePost(webhookData, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **webhookData** | [**WebhookData**](WebhookData.md)| Entidade &#x60;WebhookData&#x60; | 
 **authorization** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

