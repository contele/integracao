# ApiConteleGeV2.PoiContacts

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Nome do contato | [optional] 
**phone** | **String** | Telefone do Contato | [optional] 
**email** | **String** | E-mail do contato | [optional] 


