# ApiConteleGeV2.Portifolio

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Identificador da carteira | [optional] 
**name** | **String** | Nome da carteira | [optional] 


