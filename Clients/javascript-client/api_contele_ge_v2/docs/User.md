# ApiConteleGeV2.User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Identificador único | [optional] 
**name** | **String** | Nome da Categoria | [optional] 
**username** | **String** | E-mail do usuário | [optional] 
**document** | **String** | CPF/ID do usuário | [optional] 
**location** | [**UserLocation**](UserLocation.md) |  | [optional] 
**portifolios** | [**[UserPortifolios]**](UserPortifolios.md) |  | [optional] 
**address** | [**UserAddress**](UserAddress.md) |  | [optional] 


