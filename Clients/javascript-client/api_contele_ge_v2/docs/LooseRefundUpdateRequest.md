# ApiConteleGeV2.LooseRefundUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | refund id (required on batch update) | [optional] 
**state** | **String** | refund state | 
**paymentDate** | **String** | refund payment date | [optional] 


<a name="StateEnum"></a>
## Enum: StateEnum


* `accepted` (value: `"accepted"`)

* `refused` (value: `"refused"`)

* `paid` (value: `"paid"`)

* `processed` (value: `"processed"`)




