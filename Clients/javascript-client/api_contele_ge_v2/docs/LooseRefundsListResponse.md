# ApiConteleGeV2.LooseRefundsListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **String** | Numero da página | 
**perPage** | **String** | Numero de itens por página | 
**refunds** | [**[LooseRefund]**](LooseRefund.md) |  | 


