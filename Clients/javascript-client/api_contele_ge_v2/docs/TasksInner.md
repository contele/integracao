# ApiConteleGeV2.TasksInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**id** | **String** | Identificador único da tarefa | [optional] 
**poiId** | **String** | Identificador do local em que a tarefa será executada | 
**customPoiId** | **String** | Identificador externo do local em que a tarefa será executada | [optional] 
**customId** | **String** | Identificador externo - uso livre | [optional] 
**userId** | **String** | Identificador do funcionário no sistema Contele | [optional] 
**timezone** | **String** | Timezone em que a tarefa deverá ser criada | 
**datetime** | **String** | Data e hora da tarefa | 
**withTime** | **Boolean** | Define se a visita possui ou não horário | [optional] 
**status** | **String** | Possíveis status da tarefa  - pending - Não inciada - ongoing - Em andamento  - completed - Finalizada - cancelled - Cancelada - refused - Recusada pelo funcionário - deleted - Deletada - lost - Perdida  | [optional] 
**checkoutTime** | **String** | Horário de checkout da tarefa | [optional] 
**checkinTime** | **String** | Horário de checkin da tarefa | [optional] 
**observation** | **String** | Observação da Tarefa | [optional] 
**comments** | **String** | Comentário da tarefa | [optional] 
**justification** | **String** | justificativa da recusa | [optional] 
**os** | **String** | Número da ordem de serviço | [optional] 
**forms** | **[String]** |  | [optional] 
**photos** | **[String]** |  | [optional] 


