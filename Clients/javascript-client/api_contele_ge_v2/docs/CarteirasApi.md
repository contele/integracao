# ApiConteleGeV2.CarteirasApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**poisPoiIdPortifoliosGet**](CarteirasApi.md#poisPoiIdPortifoliosGet) | **GET** /pois/{poiId}/portifolios | 
[**portifoliosDelete**](CarteirasApi.md#portifoliosDelete) | **DELETE** /portifolios | 
[**portifoliosGet**](CarteirasApi.md#portifoliosGet) | **GET** /portifolios | 
[**portifoliosPortifolioIdPoisGet**](CarteirasApi.md#portifoliosPortifolioIdPoisGet) | **GET** /portifolios/{portifolioId}/pois | 
[**portifoliosPut**](CarteirasApi.md#portifoliosPut) | **PUT** /portifolios | 


<a name="poisPoiIdPortifoliosGet"></a>
# **poisPoiIdPortifoliosGet**
> Portifolio poisPoiIdPortifoliosGet(poiId, authorization)



Retorna as carteiras cadastradas no sistema

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CarteirasApi();

var poiId = "poiId_example"; // String | 

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.poisPoiIdPortifoliosGet(poiId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poiId** | **String**|  | 
 **authorization** | **String**|  | 

### Return type

[**Portifolio**](Portifolio.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="portifoliosDelete"></a>
# **portifoliosDelete**
> portifoliosDelete(authorizationportifolioRelation)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CarteirasApi();

var authorization = "authorization_example"; // String | 

var portifolioRelation = new ApiConteleGeV2.PortifolioRelation(); // PortifolioRelation | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.portifoliosDelete(authorizationportifolioRelation, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **portifolioRelation** | [**PortifolioRelation**](PortifolioRelation.md)|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

<a name="portifoliosGet"></a>
# **portifoliosGet**
> Portifolios portifoliosGet(authorization)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CarteirasApi();

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.portifoliosGet(authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 

### Return type

[**Portifolios**](Portifolios.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="portifoliosPortifolioIdPoisGet"></a>
# **portifoliosPortifolioIdPoisGet**
> Pois portifoliosPortifolioIdPoisGet(portifolioId, authorization)



Retorna os locais de uma determinada carteira

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CarteirasApi();

var portifolioId = "portifolioId_example"; // String | 

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.portifoliosPortifolioIdPoisGet(portifolioId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **portifolioId** | **String**|  | 
 **authorization** | **String**|  | 

### Return type

[**Pois**](Pois.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="portifoliosPut"></a>
# **portifoliosPut**
> portifoliosPut(authorizationportifolioRelation)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CarteirasApi();

var authorization = "authorization_example"; // String | 

var portifolioRelation = new ApiConteleGeV2.PortifolioRelation(); // PortifolioRelation | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.portifoliosPut(authorizationportifolioRelation, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **portifolioRelation** | [**PortifolioRelation**](PortifolioRelation.md)|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

