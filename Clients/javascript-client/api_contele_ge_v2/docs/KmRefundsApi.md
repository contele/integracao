# ApiConteleGeV2.KmRefundsApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getKmRefund**](KmRefundsApi.md#getKmRefund) | **GET** /refunds/km/{refundId} | get single refund by id
[**getLooseRefunds**](KmRefundsApi.md#getLooseRefunds) | **GET** /refunds/km | get loose refunds list


<a name="getKmRefund"></a>
# **getKmRefund**
> KmRefund getKmRefund(authorizationrefundId)

get single refund by id

Endpoint to return existing refund.

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.KmRefundsApi();

var authorization = "authorization_example"; // String | 

var refundId = "refundId_example"; // String | unique refund id


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getKmRefund(authorizationrefundId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **refundId** | **String**| unique refund id | 

### Return type

[**KmRefund**](KmRefund.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLooseRefunds"></a>
# **getLooseRefunds**
> KmRefundsListResponse getLooseRefunds(authorization, opts)

get loose refunds list

Endpoint to return existing refunds list.

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.KmRefundsApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'refundId': "refundId_example", // String | unique refund id
  'userId': "userId_example", // String | unique user id
  'startDate': "startDate_example", // String | refund date start date range (field:date)
  'endDate': "endDate_example", // String | refund end date range (field:date)
  'page': 1, // Number | page
  'perPage': 50 // Number | per page limit
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getLooseRefunds(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **refundId** | **String**| unique refund id | [optional] 
 **userId** | **String**| unique user id | [optional] 
 **startDate** | **String**| refund date start date range (field:date) | [optional] 
 **endDate** | **String**| refund end date range (field:date) | [optional] 
 **page** | **Number**| page | [optional] [default to 1]
 **perPage** | **Number**| per page limit | [optional] [default to 50]

### Return type

[**KmRefundsListResponse**](KmRefundsListResponse.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

