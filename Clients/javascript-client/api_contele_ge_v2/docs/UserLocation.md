# ApiConteleGeV2.UserLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **String** | Latitude da última posição | [optional] 
**longitude** | **String** | Longitude da última posição | [optional] 
**lastUpdate** | **String** | Data da última localização | [optional] 


