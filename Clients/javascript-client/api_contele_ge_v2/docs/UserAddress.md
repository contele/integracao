# ApiConteleGeV2.UserAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**neighborhood** | **String** | Bairro | [optional] 
**street** | **String** | Logradouro | [optional] 
**state** | **String** | Sigla do Estado | [optional] 
**zipcode** | **String** | CEP | [optional] 
**_number** | **String** | número da residência | [optional] 
**complement** | **String** | Complemento do endereço | [optional] 
**city** | **String** | Cidade | [optional] 
**location** | [**UserAddressLocation**](UserAddressLocation.md) |  | [optional] 


