# ApiConteleGeV2.KmRefundsListResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **Number** | request page parameter | 
**perPage** | **Number** | request perPage parameter | 
**refunds** | [**[KmRefund]**](KmRefund.md) | refunds list | 


