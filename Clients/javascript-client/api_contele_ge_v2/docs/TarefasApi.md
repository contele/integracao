# ApiConteleGeV2.TarefasApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**tasksBatchDelete**](TarefasApi.md#tasksBatchDelete) | **DELETE** /tasks/batch | 
[**tasksGet**](TarefasApi.md#tasksGet) | **GET** /tasks | 
[**tasksPost**](TarefasApi.md#tasksPost) | **POST** /tasks | 
[**tasksTaskIdGet**](TarefasApi.md#tasksTaskIdGet) | **GET** /tasks/{taskId} | 
[**tasksTaskIdPut**](TarefasApi.md#tasksTaskIdPut) | **PUT** /tasks/{taskId} | 


<a name="tasksBatchDelete"></a>
# **tasksBatchDelete**
> tasksBatchDelete(authorization, opts)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.TarefasApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'userId': "userId_example", // String | Enviar múltiplos ids de usuários  a serem apagados separados por virgula
  'usersIds': "usersIds_example", // String | 
  'endDate': "endDate_example", // String | 
  'ids': "ids_example", // String | Enviar múltiplos ids de visitas  a serem apagados separados por virgula
  'status': "status_example", // String | 
  'startDate': "startDate_example" // String | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.tasksBatchDelete(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **userId** | **String**| Enviar múltiplos ids de usuários  a serem apagados separados por virgula | [optional] 
 **usersIds** | **String**|  | [optional] 
 **endDate** | **String**|  | [optional] 
 **ids** | **String**| Enviar múltiplos ids de visitas  a serem apagados separados por virgula | [optional] 
 **status** | **String**|  | [optional] 
 **startDate** | **String**|  | [optional] 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="tasksGet"></a>
# **tasksGet**
> Tasks tasksGet(timezone, authorization, opts)



Gets &#x60;Task&#x60; objects.  Optional query param of **size** determines size of returned array 

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.TarefasApi();

var timezone = "timezone_example"; // String | Timezone do usuário

var authorization = "authorization_example"; // String | 

var opts = { 
  'poiId': "poiId_example", // String | 
  'toDate': "toDate_example", // String | Até essa data
  'userId': "userId_example", // String | 
  'perPage': "perPage_example", // String | Quantidade de itens por página
  'include': "include_example", // String | Permite incluir dados complementares. Ex: forms
  'page': "page_example", // String | Número da página
  'sinceDate': "sinceDate_example" // String | A partir dessa data
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.tasksGet(timezone, authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timezone** | **String**| Timezone do usuário | 
 **authorization** | **String**|  | 
 **poiId** | **String**|  | [optional] 
 **toDate** | **String**| Até essa data | [optional] 
 **userId** | **String**|  | [optional] 
 **perPage** | **String**| Quantidade de itens por página | [optional] 
 **include** | **String**| Permite incluir dados complementares. Ex: forms | [optional] 
 **page** | **String**| Número da página | [optional] 
 **sinceDate** | **String**| A partir dessa data | [optional] 

### Return type

[**Tasks**](Tasks.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="tasksPost"></a>
# **tasksPost**
> Task tasksPost(authorizationtask)



Cria uma nova tarefa

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.TarefasApi();

var authorization = "authorization_example"; // String | 

var task = new ApiConteleGeV2.Task(); // Task | Entidade `Task`


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.tasksPost(authorizationtask, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **task** | [**Task**](Task.md)| Entidade &#x60;Task&#x60; | 

### Return type

[**Task**](Task.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="tasksTaskIdGet"></a>
# **tasksTaskIdGet**
> Task tasksTaskIdGet(taskId, authorization)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.TarefasApi();

var taskId = "taskId_example"; // String | 

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.tasksTaskIdGet(taskId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **String**|  | 
 **authorization** | **String**|  | 

### Return type

[**Task**](Task.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="tasksTaskIdPut"></a>
# **tasksTaskIdPut**
> tasksTaskIdPut(taskId, authorization)



Atualiza uma tarefa

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.TarefasApi();

var taskId = "taskId_example"; // String | Identificador único da tarefa

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.tasksTaskIdPut(taskId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **taskId** | **String**| Identificador único da tarefa | 
 **authorization** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

