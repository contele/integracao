# ApiConteleGeV2.CategoriasApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoriesGet**](CategoriasApi.md#categoriesGet) | **GET** /categories | 


<a name="categoriesGet"></a>
# **categoriesGet**
> categoriesGet(authorization)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.CategoriasApi();

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.categoriesGet(authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

