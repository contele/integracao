# ApiConteleGeV2.KmRefund

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | unique id | 
**value** | **Number** | refund value (with decimal places) | 
**_date** | **String** | refund original date | 
**userId** | **String** | refunds owner id | 
**coefficient** | **Number** | refund coefficient number | [optional] 
**kilometers** | **Number** | refund distance in km | [optional] 
**startRouteKind** | **String** | Tipo do inicio da rota | [optional] 
**endRouteKind** | **String** | Tipo do fim da rota | [optional] 
**endRoute** | **String** | Rota final | [optional] 
**companyCar** | **Boolean** | is user using company car? | [optional] 
**tasksIds** | **[String]** | completed tasks between start and endpoint | [optional] 
**ignoredTasks** | **[String]** | ignored tasks between start and endpoint | [optional] 
**calculateError** | **String** | Erro de calculo | [optional] 
**calculating** | **Boolean** | is refund beeing calculated? | [optional] 
**poisNotPositioned** | **String** |  | [optional] 
**createdAt** | **String** | record created at date | [optional] 
**updatedAt** | **String** | record updated at date | [optional] 


