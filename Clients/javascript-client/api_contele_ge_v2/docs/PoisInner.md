# ApiConteleGeV2.PoisInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**id** | **String** | Identificador único do Local | [optional] 
**customId** | **String** | Identificador externo - uso livre | [optional] 
**name** | **String** | Nome fantasia do local | [optional] 
**corporateName** | **String** | Razão social do local | [optional] 
**cnpjCpf** | **String** | Documento do local (CPF ou CNPJ) | [optional] 
**lat** | **Number** | Latitude do local | [optional] 
**lng** | **Number** | Longitude do local | [optional] 
**status** | **String** |  - &#x60;not_positioned&#x60;: Não posicionado - &#x60;positioned&#x60;: Posicionado - &#x60;deleted&#x60;: Deletado - &#x60;repositioned&#x60;: Reposicionado pelo funcionário - &#x60;valid&#x60;: Validado  | [optional] 
**responsibleSignature** | **String** | Responsável pela assinatura | [optional] 
**signatureDocument** | **String** | Documentó do Responsável pela assinatura | [optional] 
**address** | [**PoiAddress**](PoiAddress.md) |  | [optional] 
**contacts** | [**[PoiContacts]**](PoiContacts.md) |  | [optional] 
**categoryId** | **String** | Identificador da categoria | [optional] 
**obs** | **String** | Campo de obervação dos Locais | [optional] 


