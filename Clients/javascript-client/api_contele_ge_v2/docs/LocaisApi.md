# ApiConteleGeV2.LocaisApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**poisBatchDelete**](LocaisApi.md#poisBatchDelete) | **DELETE** /pois/batch | 
[**poisGet**](LocaisApi.md#poisGet) | **GET** /pois | 
[**poisPoiIdGet**](LocaisApi.md#poisPoiIdGet) | **GET** /pois/{poiId} | 
[**poisPoiIdPut**](LocaisApi.md#poisPoiIdPut) | **PUT** /pois/{poiId} | 
[**poisPost**](LocaisApi.md#poisPost) | **POST** /pois | 


<a name="poisBatchDelete"></a>
# **poisBatchDelete**
> poisBatchDelete(authorization, opts)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LocaisApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'corporateName': "corporateName_example", // String | 
  'street': "street_example", // String | 
  'categoryId': "categoryId_example", // String | 
  'name': "name_example", // String | 
  'neighborhood': "neighborhood_example", // String | 
  'city': "city_example", // String | 
  'status': "status_example", // String | 
  'searchTerm': "searchTerm_example", // String | 
  'cnpjCpf': "cnpjCpf_example" // String | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.poisBatchDelete(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **corporateName** | **String**|  | [optional] 
 **street** | **String**|  | [optional] 
 **categoryId** | **String**|  | [optional] 
 **name** | **String**|  | [optional] 
 **neighborhood** | **String**|  | [optional] 
 **city** | **String**|  | [optional] 
 **status** | **String**|  | [optional] 
 **searchTerm** | **String**|  | [optional] 
 **cnpjCpf** | **String**|  | [optional] 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="poisGet"></a>
# **poisGet**
> Pois poisGet(authorization, opts)



Obtem os locais 

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LocaisApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'customId': "customId_example", // String | Filtro por customId.
  'corporateName': "corporateName_example", // String | Filtro por razão social.
  'orderBy': "orderBy_example", // String | Ordena por determinado campo. Ex: name
  'id': "id_example", // String | 
  'perPage': "perPage_example", // String | Quantidade de itens por página
  'page': "page_example", // String | Número da página
  'id2': "id_example" // String | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.poisGet(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **customId** | **String**| Filtro por customId. | [optional] 
 **corporateName** | **String**| Filtro por razão social. | [optional] 
 **orderBy** | **String**| Ordena por determinado campo. Ex: name | [optional] 
 **id** | **String**|  | [optional] 
 **perPage** | **String**| Quantidade de itens por página | [optional] 
 **page** | **String**| Número da página | [optional] 
 **id2** | **String**|  | [optional] 

### Return type

[**Pois**](Pois.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="poisPoiIdGet"></a>
# **poisPoiIdGet**
> Poi poisPoiIdGet(poiId, authorization)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LocaisApi();

var poiId = "poiId_example"; // String | 

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.poisPoiIdGet(poiId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poiId** | **String**|  | 
 **authorization** | **String**|  | 

### Return type

[**Poi**](Poi.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="poisPoiIdPut"></a>
# **poisPoiIdPut**
> poisPoiIdPut(poiId, authorization)



Atualiza um local

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LocaisApi();

var poiId = "poiId_example"; // String | Identificador único do local

var authorization = "authorization_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.poisPoiIdPut(poiId, authorization, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **poiId** | **String**| Identificador único do local | 
 **authorization** | **String**|  | 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="poisPost"></a>
# **poisPost**
> Poi poisPost(authorizationpoi)



Cria um novo local

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LocaisApi();

var authorization = "authorization_example"; // String | 

var poi = new ApiConteleGeV2.Poi(); // Poi | Entidade `Local`


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.poisPost(authorizationpoi, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **poi** | [**Poi**](Poi.md)| Entidade &#x60;Local&#x60; | 

### Return type

[**Poi**](Poi.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

