# ApiConteleGeV2.WebhookData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | Tipo do dado tratado pelo webhook    &#x60;local&#x60; - ação referente a um local    &#x60;task&#x60; - ação referente a uma tarefa  | [optional] 
**action** | **String** | Tipo da ação    - create - edit - delete  | [optional] 
**data** | **Object** | Representação do objeto que foi alterado - &#x60;type&#x60; | [optional] 


