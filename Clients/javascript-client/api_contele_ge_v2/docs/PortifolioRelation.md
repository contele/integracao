# ApiConteleGeV2.PortifolioRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**portifoliosIds** | **String** | Id do portifolio da relação | [optional] 
**poisIds** | **[String]** | Id do portifolio da relação | [optional] 


