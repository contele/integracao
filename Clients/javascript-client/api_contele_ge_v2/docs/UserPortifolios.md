# ApiConteleGeV2.UserPortifolios

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | ID da carteira | [optional] 
**name** | **String** | Nome da carteira | [optional] 


