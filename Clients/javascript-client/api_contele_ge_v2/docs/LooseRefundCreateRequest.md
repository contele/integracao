# ApiConteleGeV2.LooseRefundCreateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**value** | **Number** | refund value (with decimal places) | 
**state** | **String** | refund state | [optional] 
**_date** | **String** | refund original date | 
**type** | **String** | refund category description if category matches the following regex: &#39;/^card_?/ig&#39;, &#39;refundType&#39; will be &#39;non_refundable&#39; and the match will be removed from string. otherwise &#39;refundType&#39; will be &#39;refundable&#39; if &#39;refundType&#39; already is in payload and it&#39;s value is different from the rule, the response will be 4xx.  | [optional] [default to &#39;unknown&#39;]
**userId** | **String** | refunds owner id | 
**currency** | **String** | refund ISO currency (BRL, USA, etc.) | [optional] [default to &#39;BRL&#39;]
**paymentDate** | **String** | refund payment date | [optional] 
**refundType** | **String** | refund type | [optional] 
**taskId** | **String** | associated task id | [optional] 
**poiId** | **String** | associated poi id | [optional] 


<a name="StateEnum"></a>
## Enum: StateEnum


* `accepted` (value: `"accepted"`)

* `refused` (value: `"refused"`)

* `paid` (value: `"paid"`)

* `processed` (value: `"processed"`)




<a name="TypeEnum"></a>
## Enum: TypeEnum


* `commuting` (value: `"commuting"`)

* `equipaments` (value: `"equipaments"`)

* `parking` (value: `"parking"`)

* `toll` (value: `"toll"`)

* `food` (value: `"food"`)

* `accommodation` (value: `"accommodation"`)

* `fuel` (value: `"fuel"`)

* `card` (value: `"card"`)

* `unknown` (value: `"unknown"`)

* `post_offices` (value: `"post_offices"`)

* `others` (value: `"others"`)

* `relationship` (value: `"relationship"`)

* `incentive` (value: `"incentive"`)

* `event` (value: `"event"`)




<a name="RefundTypeEnum"></a>
## Enum: RefundTypeEnum


* `refundable` (value: `"refundable"`)

* `non_refundable` (value: `"non_refundable"`)




