# ApiConteleGeV2.LooseRefundsApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createLooseRefund**](LooseRefundsApi.md#createLooseRefund) | **POST** /refunds | create new loose refund
[**getLooseRefund**](LooseRefundsApi.md#getLooseRefund) | **GET** /refunds/{refundId} | get single refund by id
[**refundsGet**](LooseRefundsApi.md#refundsGet) | **GET** /refunds | 
[**updateLooseRefund**](LooseRefundsApi.md#updateLooseRefund) | **PUT** /refunds/{refundId} | update existing refund


<a name="createLooseRefund"></a>
# **createLooseRefund**
> LooseRefundCreateRequest createLooseRefund(authorizationlooseRefund)

create new loose refund

Endpoint to create new refund.

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LooseRefundsApi();

var authorization = "authorization_example"; // String | 

var looseRefund = new ApiConteleGeV2.LooseRefundCreateRequest(); // LooseRefundCreateRequest | new refund payload


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.createLooseRefund(authorizationlooseRefund, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **looseRefund** | [**LooseRefundCreateRequest**](LooseRefundCreateRequest.md)| new refund payload | 

### Return type

[**LooseRefundCreateRequest**](LooseRefundCreateRequest.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="getLooseRefund"></a>
# **getLooseRefund**
> LooseRefund getLooseRefund(authorizationrefundId)

get single refund by id

Endpoint to return existing refund.

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LooseRefundsApi();

var authorization = "authorization_example"; // String | 

var refundId = "refundId_example"; // String | unique refund id


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.getLooseRefund(authorizationrefundId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **refundId** | **String**| unique refund id | 

### Return type

[**LooseRefund**](LooseRefund.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="refundsGet"></a>
# **refundsGet**
> LooseRefundsListResponse refundsGet(authorization, opts)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LooseRefundsApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'state': "state_example", // String | 
  'userId': "userId_example", // String | 
  'type': "type_example", // String | 
  'endDate': "endDate_example", // String | 
  'perPage': "perPage_example", // String | 
  'page': "page_example", // String | 
  'refundType': "refundType_example", // String | 
  'startDate': "startDate_example", // String | 
  'status': "status_example", // String | 
  'taskId': "taskId_example", // String | associated task id
  'poiId': "poiId_example", // String | associated poi id
  'refundsWithPoi': false, // Boolean | refunds where poi is not null
  'refundsWithTask': false // Boolean | refunds where task is not null
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.refundsGet(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **state** | **String**|  | [optional] 
 **userId** | **String**|  | [optional] 
 **type** | **String**|  | [optional] 
 **endDate** | **String**|  | [optional] 
 **perPage** | **String**|  | [optional] 
 **page** | **String**|  | [optional] 
 **refundType** | **String**|  | [optional] 
 **startDate** | **String**|  | [optional] 
 **status** | **String**|  | [optional] 
 **taskId** | **String**| associated task id | [optional] 
 **poiId** | **String**| associated poi id | [optional] 
 **refundsWithPoi** | **Boolean**| refunds where poi is not null | [optional] [default to false]
 **refundsWithTask** | **Boolean**| refunds where task is not null | [optional] [default to false]

### Return type

[**LooseRefundsListResponse**](LooseRefundsListResponse.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateLooseRefund"></a>
# **updateLooseRefund**
> LooseRefund updateLooseRefund(authorizationrefundId, body)

update existing refund

Endpoint to update existing refund.

### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.LooseRefundsApi();

var authorization = "authorization_example"; // String | 

var refundId = "refundId_example"; // String | unique refund id

var body = new ApiConteleGeV2.LooseRefundUpdateRequest(); // LooseRefundUpdateRequest | refund payload


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.updateLooseRefund(authorizationrefundId, body, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **refundId** | **String**| unique refund id | 
 **body** | [**LooseRefundUpdateRequest**](LooseRefundUpdateRequest.md)| refund payload | 

### Return type

[**LooseRefund**](LooseRefund.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

