# ApiConteleGeV2.FormsApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**formsGet**](FormsApi.md#formsGet) | **GET** /forms | 


<a name="formsGet"></a>
# **formsGet**
> formsGet(opts)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.FormsApi();

var opts = { 
  'authorization': "authorization_example", // String | 
  'status': "status_example", // String | 
  'formModelId': "formModelId_example", // String | 
  'taskId': "taskId_example" // String | 
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully.');
  }
};
apiInstance.formsGet(opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | [optional] 
 **status** | **String**|  | [optional] 
 **formModelId** | **String**|  | [optional] 
 **taskId** | **String**|  | [optional] 

### Return type

null (empty response body)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

