# ApiConteleGeV2.UserAddressLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **String** | Latitude | [optional] 
**longitude** | **String** | Longitude | [optional] 


