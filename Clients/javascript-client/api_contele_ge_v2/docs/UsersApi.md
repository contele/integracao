# ApiConteleGeV2.UsersApi

All URIs are relative to *https://integration.contelege.com.br/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersGet**](UsersApi.md#usersGet) | **GET** /users | 


<a name="usersGet"></a>
# **usersGet**
> Users usersGet(authorization, opts)



### Example
```javascript
var ApiConteleGeV2 = require('api_contele_ge_v2');
var defaultClient = ApiConteleGeV2.ApiClient.default;

// Configure API key authorization: x-api-key
var x-api-key = defaultClient.authentications['x-api-key'];
x-api-key.apiKey = 'YOUR API KEY';
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//x-api-key.apiKeyPrefix = 'Token';

var apiInstance = new ApiConteleGeV2.UsersApi();

var authorization = "authorization_example"; // String | 

var opts = { 
  'username': "username_example", // String | Valor exato do username cadastrado para o respectivo usuário.
  'include': "include_example" // String | Utilizar 'lastLocation' para última localização e 'portifolios' para carteiras
};

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.usersGet(authorization, opts, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**|  | 
 **username** | **String**| Valor exato do username cadastrado para o respectivo usuário. | [optional] 
 **include** | **String**| Utilizar &#39;lastLocation&#39; para última localização e &#39;portifolios&#39; para carteiras | [optional] 

### Return type

[**Users**](Users.md)

### Authorization

[x-api-key](../README.md#x-api-key)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

