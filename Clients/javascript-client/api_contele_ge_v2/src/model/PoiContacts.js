/**
 * API Contele GE V2
 * Documentação de integração com a API do contele Gestor de Equipes
 *
 * OpenAPI spec version: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'));
  } else {
    // Browser globals (root is window)
    if (!root.ApiConteleGeV2) {
      root.ApiConteleGeV2 = {};
    }
    root.ApiConteleGeV2.PoiContacts = factory(root.ApiConteleGeV2.ApiClient);
  }
}(this, function(ApiClient) {
  'use strict';




  /**
   * The PoiContacts model module.
   * @module model/PoiContacts
   * @version 2.0.0
   */

  /**
   * Constructs a new <code>PoiContacts</code>.
   * @alias module:model/PoiContacts
   * @class
   */
  var exports = function() {
    var _this = this;




  };

  /**
   * Constructs a <code>PoiContacts</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/PoiContacts} obj Optional instance to populate.
   * @return {module:model/PoiContacts} The populated <code>PoiContacts</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('name')) {
        obj['name'] = ApiClient.convertToType(data['name'], 'String');
      }
      if (data.hasOwnProperty('phone')) {
        obj['phone'] = ApiClient.convertToType(data['phone'], 'String');
      }
      if (data.hasOwnProperty('email')) {
        obj['email'] = ApiClient.convertToType(data['email'], 'String');
      }
    }
    return obj;
  }

  /**
   * Nome do contato
   * @member {String} name
   */
  exports.prototype['name'] = undefined;
  /**
   * Telefone do Contato
   * @member {String} phone
   */
  exports.prototype['phone'] = undefined;
  /**
   * E-mail do contato
   * @member {String} email
   */
  exports.prototype['email'] = undefined;



  return exports;
}));


