/**
 * API Contele GE V2
 * Documentação de integração com a API do contele Gestor de Equipes
 *
 * OpenAPI spec version: 2.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/KmRefund', 'model/KmRefundsListResponse', 'model/LooseRefund', 'model/LooseRefundCreateRequest', 'model/LooseRefundUpdateRequest', 'model/LooseRefundsListResponse', 'model/Poi', 'model/PoiAddress', 'model/PoiContacts', 'model/Pois', 'model/PoisInner', 'model/Portifolio', 'model/PortifolioRelation', 'model/Portifolios', 'model/Task', 'model/Tasks', 'model/TasksInner', 'model/User', 'model/UserAddress', 'model/UserAddressLocation', 'model/UserLocation', 'model/UserPortifolios', 'model/Users', 'model/WebhookData', 'api/CarteirasApi', 'api/CategoriasApi', 'api/FormsApi', 'api/KmRefundsApi', 'api/LocaisApi', 'api/LooseRefundsApi', 'api/TarefasApi', 'api/UsersApi', 'api/WebhookApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/KmRefund'), require('./model/KmRefundsListResponse'), require('./model/LooseRefund'), require('./model/LooseRefundCreateRequest'), require('./model/LooseRefundUpdateRequest'), require('./model/LooseRefundsListResponse'), require('./model/Poi'), require('./model/PoiAddress'), require('./model/PoiContacts'), require('./model/Pois'), require('./model/PoisInner'), require('./model/Portifolio'), require('./model/PortifolioRelation'), require('./model/Portifolios'), require('./model/Task'), require('./model/Tasks'), require('./model/TasksInner'), require('./model/User'), require('./model/UserAddress'), require('./model/UserAddressLocation'), require('./model/UserLocation'), require('./model/UserPortifolios'), require('./model/Users'), require('./model/WebhookData'), require('./api/CarteirasApi'), require('./api/CategoriasApi'), require('./api/FormsApi'), require('./api/KmRefundsApi'), require('./api/LocaisApi'), require('./api/LooseRefundsApi'), require('./api/TarefasApi'), require('./api/UsersApi'), require('./api/WebhookApi'));
  }
}(function(ApiClient, KmRefund, KmRefundsListResponse, LooseRefund, LooseRefundCreateRequest, LooseRefundUpdateRequest, LooseRefundsListResponse, Poi, PoiAddress, PoiContacts, Pois, PoisInner, Portifolio, PortifolioRelation, Portifolios, Task, Tasks, TasksInner, User, UserAddress, UserAddressLocation, UserLocation, UserPortifolios, Users, WebhookData, CarteirasApi, CategoriasApi, FormsApi, KmRefundsApi, LocaisApi, LooseRefundsApi, TarefasApi, UsersApi, WebhookApi) {
  'use strict';

  /**
   * Documentao_de_integrao_com_a_API_do_contele_Gestor_de_Equipes.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var ApiConteleGeV2 = require('index'); // See note below*.
   * var xxxSvc = new ApiConteleGeV2.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new ApiConteleGeV2.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
   * and put the application logic within the callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new ApiConteleGeV2.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new ApiConteleGeV2.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 2.0.0
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The KmRefund model constructor.
     * @property {module:model/KmRefund}
     */
    KmRefund: KmRefund,
    /**
     * The KmRefundsListResponse model constructor.
     * @property {module:model/KmRefundsListResponse}
     */
    KmRefundsListResponse: KmRefundsListResponse,
    /**
     * The LooseRefund model constructor.
     * @property {module:model/LooseRefund}
     */
    LooseRefund: LooseRefund,
    /**
     * The LooseRefundCreateRequest model constructor.
     * @property {module:model/LooseRefundCreateRequest}
     */
    LooseRefundCreateRequest: LooseRefundCreateRequest,
    /**
     * The LooseRefundUpdateRequest model constructor.
     * @property {module:model/LooseRefundUpdateRequest}
     */
    LooseRefundUpdateRequest: LooseRefundUpdateRequest,
    /**
     * The LooseRefundsListResponse model constructor.
     * @property {module:model/LooseRefundsListResponse}
     */
    LooseRefundsListResponse: LooseRefundsListResponse,
    /**
     * The Poi model constructor.
     * @property {module:model/Poi}
     */
    Poi: Poi,
    /**
     * The PoiAddress model constructor.
     * @property {module:model/PoiAddress}
     */
    PoiAddress: PoiAddress,
    /**
     * The PoiContacts model constructor.
     * @property {module:model/PoiContacts}
     */
    PoiContacts: PoiContacts,
    /**
     * The Pois model constructor.
     * @property {module:model/Pois}
     */
    Pois: Pois,
    /**
     * The PoisInner model constructor.
     * @property {module:model/PoisInner}
     */
    PoisInner: PoisInner,
    /**
     * The Portifolio model constructor.
     * @property {module:model/Portifolio}
     */
    Portifolio: Portifolio,
    /**
     * The PortifolioRelation model constructor.
     * @property {module:model/PortifolioRelation}
     */
    PortifolioRelation: PortifolioRelation,
    /**
     * The Portifolios model constructor.
     * @property {module:model/Portifolios}
     */
    Portifolios: Portifolios,
    /**
     * The Task model constructor.
     * @property {module:model/Task}
     */
    Task: Task,
    /**
     * The Tasks model constructor.
     * @property {module:model/Tasks}
     */
    Tasks: Tasks,
    /**
     * The TasksInner model constructor.
     * @property {module:model/TasksInner}
     */
    TasksInner: TasksInner,
    /**
     * The User model constructor.
     * @property {module:model/User}
     */
    User: User,
    /**
     * The UserAddress model constructor.
     * @property {module:model/UserAddress}
     */
    UserAddress: UserAddress,
    /**
     * The UserAddressLocation model constructor.
     * @property {module:model/UserAddressLocation}
     */
    UserAddressLocation: UserAddressLocation,
    /**
     * The UserLocation model constructor.
     * @property {module:model/UserLocation}
     */
    UserLocation: UserLocation,
    /**
     * The UserPortifolios model constructor.
     * @property {module:model/UserPortifolios}
     */
    UserPortifolios: UserPortifolios,
    /**
     * The Users model constructor.
     * @property {module:model/Users}
     */
    Users: Users,
    /**
     * The WebhookData model constructor.
     * @property {module:model/WebhookData}
     */
    WebhookData: WebhookData,
    /**
     * The CarteirasApi service constructor.
     * @property {module:api/CarteirasApi}
     */
    CarteirasApi: CarteirasApi,
    /**
     * The CategoriasApi service constructor.
     * @property {module:api/CategoriasApi}
     */
    CategoriasApi: CategoriasApi,
    /**
     * The FormsApi service constructor.
     * @property {module:api/FormsApi}
     */
    FormsApi: FormsApi,
    /**
     * The KmRefundsApi service constructor.
     * @property {module:api/KmRefundsApi}
     */
    KmRefundsApi: KmRefundsApi,
    /**
     * The LocaisApi service constructor.
     * @property {module:api/LocaisApi}
     */
    LocaisApi: LocaisApi,
    /**
     * The LooseRefundsApi service constructor.
     * @property {module:api/LooseRefundsApi}
     */
    LooseRefundsApi: LooseRefundsApi,
    /**
     * The TarefasApi service constructor.
     * @property {module:api/TarefasApi}
     */
    TarefasApi: TarefasApi,
    /**
     * The UsersApi service constructor.
     * @property {module:api/UsersApi}
     */
    UsersApi: UsersApi,
    /**
     * The WebhookApi service constructor.
     * @property {module:api/WebhookApi}
     */
    WebhookApi: WebhookApi
  };

  return exports;
}));
